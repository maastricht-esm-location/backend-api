#!/usr/bin/env python3

from flask import Flask, request, make_response
from flask_restful import Resource, Api

from esm_backend import (create_engine, create_session, setup,
                         authenticate_user_token, authenticate_server_token,
                         list_users, user_exists, add_user, delete_user,
                         add_entries, create_default_session)

app = Flask(__name__)
api = Api(app)

def authenticate(request, session, server_auth_only=False, user_auth_only=False):
    """Returns true if the request is authenticated correctly."""
    if request.json:
        try:
            userid = int(request.json.get('userid'))
        except:
            userid = None
        token = request.json.get('token')
        if userid and token and not server_auth_only:
            return authenticate_user_token(userid, token, session)
        elif token and not user_auth_only:
            return authenticate_server_token(token, session)
    try:
        userid = int(request.args.get('userid'))
    except:
        userid = None
    token = request.args.get('token')
    if userid and token and not server_auth_only:
        return authenticate_user_token(userid, token, session)
    elif token and not user_auth_only:
        return authenticate_server_token(token, session)
    return False

class UsersList(Resource):
    def get(self):
        session = create_default_session()
        if not authenticate(request, session):
            return {'status': 'Authentication failure'}
        return {'users': list_users(session), 'status': 'Success!'}

class User(Resource):
    def get(self, user_id):
        session = create_default_session()
        if not authenticate(request, session):
            return {'status': 'Authentication failure'}
        # Validation
        try:
            int(user_id)
        except:
            return {'status': 'Malformed payload: user_id not an int'}
        return {'userexists': user_exists(user_id, session), 'status':'Success!'}

    def delete(self, user_id):
        session = create_default_session()
        if not authenticate(request, session, user_auth_only=True):
            return {'status': 'Authentication failure'}

        # Validation
        try:
            int(userid)
        except:
            return {'status': 'Malformed payload: userid not an int'}

        try:
            delete_user(user_id, session)
        except:
            return {'status': 'Server error'}
        return {'status': 'Success!'}

class NewUser(Resource):
    def post(self):
        session = create_default_session()
        if not authenticate(request, session, server_auth_only=True):
            return {'status': 'Authentication failure'}

        # Validation
        userid = request.json.get('userid')
        if not userid:
            return {'status': 'Malformed payload: no userid entry'}
        try:
            int(userid)
        except:
            return {'status': 'Malformed payload: userid not an int'}

        try:
            token = add_user(userid, session)
        except:
            return {'status': 'Server error'}
        return {'url': request.url_root + "users/" + userid,
                'token': token, 'status': 'Success!'}

class AddEntries(Resource):
    def post(self):
        session = create_default_session()
        if not authenticate(request, session, user_auth_only=True):
            return {'status': 'Authentication failure'}

        # Validation
        userid = request.json.get('userid')
        if not userid:
            return {'status': 'Malformed payload: no userid entry'}
        entries = request.json.get('entries')
        if not entries:
            return {'status': 'Malformed payload: no entries entry'}
        for entry in entries:
            try:
                int(entry.get('timestamp'))
                float(entry.get('latitude'))
                float(entry.get('longitude'))
            except:
                return {'status': 'Malformed payload: error parsing entry'}
       
        # Execution
        try:
            add_entries(userid, entries, session)
        except Exception as e:
            return {'status': 'Server error: ' + str(e)}
        return {'status': 'Success!'}

class ListResources(Resource):
    def get(self):
        auth_status = 'Unauthenticated'
        session = create_default_session()
        if authenticate(request, session):
            auth_status = 'Authenticated'

        return {'status': 'Success!',
                'resources': ['/users', '/users/<user_id>', '/new-user',
                              '/add-entries'],
                'auth': auth_status}

api.add_resource(UsersList, '/users')
api.add_resource(User, '/users/<user_id>')
api.add_resource(NewUser, '/new-user')
api.add_resource(AddEntries, '/add-entries')
api.add_resource(ListResources, '/')

engine = create_engine()
setup(engine)

if __name__ == '__main__':
    app.run(debug=True)
