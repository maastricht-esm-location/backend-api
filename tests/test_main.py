#!/usr/bin/env python3
# This pytest test examines if the logic parts of the backend are working

# Hack to change the current working directory and paths so
# that the main modules are easily accessible
import sys
import os
_script_path = os.path.abspath(__file__)
_script_directory = os.path.dirname(_script_path)
_sigman_root_directory = os.path.dirname(_script_directory)
os.chdir(_script_directory)
sys.path.append(_sigman_root_directory)

import pytest
from esm_backend import main, database
from esm_backend.database import create_session, create_engine

def create_test_engine():
    return create_engine(filename=":memory:")

@pytest.fixture
def blank_engine():
    return create_test_engine()

@pytest.fixture
def clean_session():
    engine = create_test_engine()
    main.setup(engine, token='big-red-cat')
    return create_session(engine)

@pytest.fixture
def user_session():
    session = clean_session()
    main.add_user(3, session)
    main.add_user(5, session, token='golden-snake')
    main.add_user(324, session)
    return session

@pytest.fixture
def entries_list():
    list_ = [
        {
            'timestamp': 1540683883,
            'latitude': 50.8,
            'longitude': 5.68
        }, {
            'timestamp': 1540683832,
            'latitude': 50.4,
            'longitude': 5.58
        }, {
            'timestamp': 1540683833,
            'latitude': 50.3,
            'longitude': 5.53
        }
    ]
    return list_

@pytest.fixture
def complex_session():
    session = user_session()
    main.add_entries(5, entries_list(), session)
    return session

def test_setup(blank_engine):
    main.setup(blank_engine)
    session = create_session(blank_engine)
    assert session.query(database.Token).count() == 1

def test_list_tokens(clean_session):
    assert 'big-red-cat' in main.list_server_tokens(clean_session)

def test_add_user(clean_session):
    main.add_user(5, clean_session)
    assert clean_session.query(database.User).count() == 1

def test_list_users(user_session):
    assert user_session.query(database.User).count() == 3
    assert 5 in main.list_users(user_session)
    assert 324 in main.list_users(user_session)

def test_user_exists(user_session):
    assert main.user_exists(3, user_session)

def test_add_entries(user_session, entries_list):
    main.add_entries(5, entries_list, user_session)
    assert len(main.get_entries(5, user_session)) == len(entries_list)

def test_delete_user(complex_session, entries_list):
    main.delete_user(5, complex_session)
    assert not main.user_exists(5, complex_session)
    assert len(main.get_entries(5, complex_session)) == 0

def test_server_token_auth(clean_session):
    assert main.authenticate_server_token('big-red-cat', clean_session)
    assert not main.authenticate_server_token('funky-squirrel', clean_session)

def test_user_token_auth(user_session):
    assert main.authenticate_user_token(5, 'golden-snake', user_session)
    assert not main.authenticate_user_token(3, 'golden-snake', user_session)
    assert not main.authenticate_user_token(5, 'great-lyricist', user_session)
