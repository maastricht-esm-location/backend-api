# Backend
The ESM geolocation tracking backend is a Dockerized Python 3 flask application with
an SQLite database.

### Technical overview 
The database contains users as integer ids with short, generated plaintext tokens and
a series of timestamped geolocation entries for each user.

All users have an id and a token generated when they are first registered. To
register a user new, the default server token must be used. This is to avoid
_completely_ unauthorized people from abusing the service, but people who are given
the token should be trusted nonetheless.

### Running it
To run the server we may simply start up the docker image. After building it
```
docker build . -t esm_geolocation
```
we may simply run
```
docker run -p 8080:80 esm_geolocation
```
it will now be running on the port 8080.

Environment variables to consider:
 - `ESM_GEOLOCATION_DEFAULT_TOKEN` - default server token. Default: `simplistic-specialist`
 - `ESM_GEOLOCATION_DATA_DIR` - data directory in the container. Default: `data`

To launch the server with persistence and a custom token run:
```
docker run -p 8080:80 \
           -v /absolute/path/to/data:/app/data:rw \
           -e "ESM_GEOLOCATION_DEFAULT_TOKEN=token" esm_geolocation
```

## Endpoints
Every request must be authenticated with either the server token or a user id + user
token combination. If it is a `GET` request, the authentication form must be encoded
in the url. If it is a `POST` request, it must be in the json payload. Uploading
location entries can only be done with a user id + token combination. Allowed
authentication methods:
 - `token=<server-token>` for authentication with the server token
 - `userid=<user-id>` and `token=<user-token>` for authentication with the user token

`GET` example: `https://example.com/users?token=big-red-cats`

`POST` example: `https://example.com/new-user` with the payload `{"token": "big-red-cats", "userid": "123"}`

Every response also includes a `status` value which contains an informative message.

### `/`
#### GET
This endpoint returns the list of resources, as well as information whether the
client is authenticated.

### `/users`
#### GET 
This endpoint returns a list of user ids.

Returns:
```json
{
    users: [
        {
            userid: <id>
        },
        ...
    ]
}
```

### `/users/<id>`
#### GET
This endpoint returns whether a user with the given id exists.

Returns:
```json
{
    userexists: <true/false>
}
```

#### DELETE
This endpoint requests the deletion of a user's data, including the user and their
token. It may only be authenticated with the user id + user token combination.

### `/new-user`
#### POST
This endpoint creates a new user with a given ID. It may only be authenticated with a
server token.

Takes:
```json
{
    token: <token>,
    userid: <id>
}
```
Returns:
```json
{
    url: <url-to-/users/[new-user-id]>,
    token: <new-user-token>
}
```

### `/add-entries`
#### POST
This endpoint adds entries to the database. It may only be authenticated using a
user id + user token combination.

Takes:
```json
{
    token: <token>,
    userid: <id>, 
    entries: [
        {
            timestamp: <unix-timestamp>,
            latitude: <lat>,
            longitude: <long>
        }, 
        ...
    ]
}
```

## Database schema
The data will be stored in a small SQLite database for simplicity sake.

### Tokens table
This table will contain permitted server tokens.
```
tokens (
    token text primary key
)
```

### Users table
This table will contain user ids and their private tokens.
```
users (
    id int primary key
    token text
)
```

### Location table
This table will contain timestamped location entries with references to users.
```
locations (
    userid int foreign key
    timestamp int
    latitude real
    longitude real
)
```

## Credits
 - Word list courtesy of [Desi Quintans](http://www.desiquintans.com/nounlist)
