FROM tiangolo/uwsgi-nginx-flask:python3.7

COPY ./requirements.txt .

RUN pip3 install -r requirements.txt

COPY ./app.py /app/main.py
COPY ./esm_backend /app/esm_backend
COPY ./nouns.txt /app/nouns.txt

COPY ./uwsgi.ini /app/uwsgi.ini
