"""
This module contains main working elements of the ESM backend.
"""
import random
import logging
import os

from .database import create_engine, create_session
from .database import create_all_tables
from .database import Token, User, Location

DEFAULT_TOKEN = os.environ.get('ESM_GEOLOCATION_DEFAULT_TOKEN',
                               'simplistic-specialist')

logger = logging.getLogger('main')

def generate_token(length=3):
    """Generates a `length` long token"""
    with open('nouns.txt') as word_file:
        words_list = [word.strip() for word in word_file.readlines()]
        words = [random.choice(words_list) for i in range(length)]
    return "-".join(words)

def setup(engine, token=None):
    """Opens up a database and generates the main token."""
    session = create_session(engine)
    create_all_tables(engine)
    if session.query(Token).count() == 0:
        if token is None:
            token = DEFAULT_TOKEN
        logger.info("Default token is {}".format(token))
        token = Token(token=token)
        session.add(token)
        session.commit()

def list_server_tokens(session):
    tokens = session.query(Token.token).all()
    return [token[0] for token in tokens]

def authenticate_server_token(token, session):
    if session.query(Token.token).filter_by(token=token).first():
        return True
    return False

def authenticate_user_token(user_id, token, session):
    user = session.query(User).filter_by(id=user_id).first()
    return user.token == token

def list_users(session):
    """Returns a list of user ids."""
    users = [user.id for user in session.query(User.id).all()]
    return users

def user_exists(user_id, session):
    """Returns whether a user exists."""
    return session.query(User.id).filter_by(id=user_id).count() > 0

def add_user(user_id, session, token=None):
    """Creates a user."""
    if token is None:
        token = generate_token()
    user = User(id=user_id, token=token)
    session.add(user)
    session.commit()
    logger.debug("Created a user with id {}".format(user_id))
    return token

def delete_user(user_id, session):
    """Deletes a user and all of their data."""
    user = session.query(User).filter_by(id=user_id).first()
    session.delete(user)
    session.commit()

def add_entries(user_id, entries, session):
    """Adds a user's entries."""
    for entry in entries:
        location = Location(userid=user_id, timestamp=entry.get('timestamp'),
                             latitude=entry.get('latitude'),
                             longitude=entry.get('longitude'))
        session.merge(location)
    session.commit()

def get_entries(user_id, session):
    entries = session.query(Location.userid, Location.timestamp, Location.longitude,
                            Location.latitude).filter_by(userid=user_id).all()
    clean_entries = [(entry.timestamp, entry.latitude, entry.longitude) for
                     entry in entries]
    return clean_entries
