"""
This module is responsible for fetching data from and into the database.
"""
import logging
import os

from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy import create_engine as sqlalchemy_create_engine
from sqlalchemy import Column, Integer, ForeignKey, String, Table, Float
from sqlalchemy.ext.declarative import declarative_base

logger = logging.getLogger('databases')

DATA_DIR = os.environ.get('ESM_GEOLOCATION_DATA_DIR', 'data')

def create_engine(path=os.path.join(DATA_DIR, 'db.sqlite'), echo=False):
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    engine = sqlalchemy_create_engine('sqlite:///' + path, echo=echo)
    with engine.connect() as connection:
        connection.execute('PRAGMA foreign_keys=ON')
    return engine

def create_session(engine):
    logger.debug("Connecting to: %s", engine.url.database)
    Session = scoped_session(sessionmaker(bind=engine))
    return Session()

def create_default_session():
    return create_session(create_engine())

Base = declarative_base()

class Token(Base):
    __tablename__ = 'tokens'

    token = Column(String, primary_key=True)

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    token = Column(String)

class Location(Base):
    __tablename__ = 'locations'

    userid = Column(ForeignKey('users.id', ondelete="CASCADE"), primary_key=True)
    timestamp = Column(Integer, primary_key=True)
    latitude = Column(Float)
    longitude = Column(Float)

def create_all_tables(engine):
    Base.metadata.create_all(engine)
